document.querySelector('nav').
		querySelector('.button').
		addEventListener('click', zdarzenie => {
			var przycisk= 
				zdarzenie.target.tagName.toLowerCase()==='p' ? 
				zdarzenie.target.parentNode : 
				zdarzenie.target;
			//console.log(przycisk, zdarzenie);
			
			if (!przycisk.hide) {
				/*przycisk.parentNode.style.gridTemplateColumns=`auto ${przycisk.clientWidth}px`;
				przycisk.parentNode.style.width=przycisk.clientWidth+"px";
				setTimeout(() => {
					przycisk.parentNode.style.gridTemplateColumns="0 100%";
					przycisk.children[0].innerText=">";
				}, 1000);*/
				przycisk.parentNode.style.transform="translate3d(-90%,0,0)";
				//przycisk.children[0].innerText=">";
				przycisk.children[0].style.transform="rotate3d(0,0,1,-180deg)";
			}
			else {
				//przycisk.parentNode.style.gridTemplateColumns=`auto ${przycisk.clientWidth}px`;
				przycisk.parentNode.style="";
				przycisk.children[0].style="";
				//przycisk.children[0].innerText="<";
				

			}
			przycisk.hide=!przycisk.hide;
		});

function generujPytania() {
	//ponieramy element naszego menu pytań, w którym będą umieszczone
	var nawigacja = document.querySelector("#poleOdpowiedzi");
	//czyścimy całą zawartość menu, która ewentualnie jeszcze się w nim znajduje
	while(nawigacja.children.length) 
		nawigacja.removeChild(nawigacja.children[nawigacja.children.length-1]);
	nawigacja.pozostaloOdpowiedzi=pytaniaOdpowiedzi.length;
	//przeskakujemy po wszystkich dodanych pytaniach
	for(let i=0;i<pytaniaOdpowiedzi.length;i++) {
		//tworzymy dowiązania/uchwyt do elementów w dokumencie
		let elem = document.createElement('div');
		let opis = document.createElement('p');
		//podajemy nazwę pytania, pod jaką będzie ono widoczne
		//w naszym menu
		opis.innerText = pytaniaOdpowiedzi[i].nazwa;
		//prawidłową odpiwiedź dokładamy do zmiennej JavaScript elementu HTML
		//dzięki temu uczestnik nie będzie jej widział w kodzie HTML 
		//UWAGA! W fazie produkcji w ogóle ta zmienna nie powinna być 
		//ustawiana a jedynie referencja do pytania z odpowiedzią po stronie 
		//serwera (obecnie nie używamy)
		elem.odp=pytaniaOdpowiedzi[i]['prawidlowa'];
		//dodajmey pola z pytaniem oraz odpowiedziami; te elementy mogą
		//być widoczne w HTML i na stronie
		elem.dataset.pytanie = pytaniaOdpowiedzi[i]['pytanie'];
		elem.dataset.odpa = pytaniaOdpowiedzi[i]['a'];
		elem.dataset.odpb = pytaniaOdpowiedzi[i]['b'];
		elem.dataset.odpc = pytaniaOdpowiedzi[i].c;
		elem.dataset.odpd = pytaniaOdpowiedzi[i].d;
		//do tak utworzonego elementu dodajemy nasłuch zdarzenia onclick
		//dzięki któremu będziemy ładować nasze pytanie do elementu właściwego
		//quizu
		elem.addEventListener('click', zdarzenie => {
			//tworzymy zmienną uchwyt (pomocniczą) dzięki której będziemy
			//mieć pewność, że pracujemy z poprawnym elementem HTML (zawierającym
			//nasze pytanie i odpowiedzi - pola dataset)
			let elem = zdarzenie.target.tagName.toLowerCase()==='p' ? zdarzenie.target.parentNode : zdarzenie.target;
			//pobieramy element z testem
			let sekcja = document.querySelector("#pytanie");
			//wrzucamy pytatanie
			sekcja.querySelector('h3').innerText = elem.dataset.pytanie;
			sekcja.pytanieNav = elem;
			sekcja.querySelectorAll('span').forEach(function(pobranySpan) {	
				//sekcja.pytanieNav.dataset.dane=pobranySpan.parentNode.children[0].value;
				pobranySpan.parentNode.children[0].checked=false;
				if (sekcja.pytanieNav.dataset.udzielonaOdp) {
					if(sekcja.pytanieNav.dataset.udzielonaOdp===pobranySpan.parentNode.children[0].value)
						pobranySpan.parentNode.children[0].checked=true;
				}
				pobranySpan.innerText=elem.dataset['odp'+pobranySpan.parentNode.children[0].value];
				pobranySpan.parentNode.children[0].addEventListener('click', zdarzenie => {
					if (!sekcja.pytanieNav.dataset.udzielonaOdp)
						nawigacja.pozostaloOdpowiedzi--;
					sekcja.pytanieNav.dataset.udzielonaOdp=zdarzenie.target.value;
					if (nawigacja.pozostaloOdpowiedzi===0)
						document.querySelector('#dzialanie button').disabled=false;
				});
			});
		});
		elem.appendChild(opis);
		nawigacja.appendChild(elem);
	}
}

(()=>{
	let a=document.createElement('script');
	//obie zaproponowane metody nadania wartości src są równorzędne z wyjątkiem,
	//że niektóre przegląraki nie chcą ustawiać niektórych atrybutów HTML
	//poprzez łącznik (kropkę). Stąd warto niekiedy zastosować metodę 
	//setAttribute (jak tutaj poniżej); pierwszy jej parametr do nazwa atrubutu
	//HTML, drugi to ustawiana wartość
	//a.src = './jscript/cookies.js';
	a.setAttribute('src','./jscript/cookies.js');
	/*
		element script, jak i niektóre pozostałe elementy HTML posiadają 
		dodatkowo zdarzenie load. Możemy je obsłużyć poprzez atrubut/metodę/właściwość
		onload 
		poniższy kod (pierwsza linia) jest tożsamy z takim kodem
		a.addEventListener('load', () => {
			
		poniższy kod jest bardzie ergonomiczny, jednak może nie być obsługiwany
		przez niekttóre przeglądarki (np. na telefonach)
		
		Zdefiniowana funkcja wykona się wtedy i tylko wtedy, kiedy dodawany
		element zostanie w pełni załadowany na stronie (w innym wypadku
		działanie skończyłoby się niepowodzeniem, gdyż nie posiadalibyśmy
		funkcji zdefiniowanych w ładowanym skrypcie)
	*/
	a.onload = () => {
		/*
			tworzymy nową zmienną o zasięgu lokalnym, która będzie zawierała
			zapis kodu wywoływanej przez nas funkcji anonimowej; zmienna i tak
			zostanie tylko w zasięgu naszej funkcji anonimowej (let) jednak dzięki
			takiemu przypisaniu będziemy ją np. przypisać jako parametr i
			odwoływać się do niej wewnątrz ciała tworzonej funkcji (przypisanej do onload)
			Funkcja przyjmuje jeden paramter, który będzie teprezentowany przez nazwę t 
		*/
		let checkCookie = (mojaFunkcja) => {
			//pobieramy element login - który na razie na stronie jest pusty
			let login = document.getElementById('login');
			//pobieramy wartość ciasteczka o nazwie gracz
			let n = getcookie('gracz');
			//console.log(n);
			//jeżeli nasze ciasteczko istnieje i zawiera jakąkolwiek wartość
			//wykonujemuy ten kod
			if (n!==undefined) {
				//wstawiamy HTML wyświetlający tekst 'Witaj, <wartość_z_ciasteczka>' oraz przycisk Wyloguj
				login.innerHTML=`<p>Witaj, ${n}</p><button>Wyloguj</button>`;
				/*
					Dla nowo utworzonego przycisku (odwołanie przez element
					login.querySelector('button'))
					dodajemy obsługę zdarzenia onclick;
					
					Jeżeli klikniemy na przycisk usuniemy ciasteczko i wymusimy 
					na stronie odświeżenie wyglądu przez wywołanie własne obecnie
					przetwarzanej funkcji; Jako parametr funkcja znowu przyjmie samą siebie
					by móc wykonać się z kolejnym zdarzeniem; jest to efekt
					tzw. rekurencji, czyli samowywołania przez funkcję. 
				*/
				login.querySelector('button').onclick = () => {deletecookie('gracz');mojaFunkcja(mojaFunkcja)};
			}	
			//jeżeli ciasteczko jednak nie zostało ustawione
			//wywołujemuy ten kod			
			else {
				//tworzymy elementy HTML jak wyżej
				login.innerHTML='<input type="text" placeholder="Wpisz nazwę użytkownika"/><button>Zaloguj</button>';
				//tym razem przycisk ustawia nam ciasteczko na 10 minut (60*10) i ponownie wywołuujemy funkcję z parametru
				login.querySelector('button').onclick = () => {setcookie('gracz',login.querySelector('input').value, 60*10);mojaFunkcja(mojaFunkcja)};
			}
		};
		//wywołujemy naszą nienazwaną funkcję poprzez zapisaną zmienną let
		//jako parametr do tak wywoływanej funkcji przypisujemy wartść, która
		//zapisana jest w zmiennej checkCookie
		//tym samym zapewniamuy jej rekurencyjność (to jej nazwa będzie wywoływana)
		checkCookie(checkCookie);
	}
	//dopiero w tym miejscu dodajemy utworzony przez nas obiekt HTML script do
	//strony HTML w ciele strony (element body)
	document.body.appendChild(a);	
})(); //nawiasy przez średnikiem gwarantują wykonanie tak utworzonego bloku kodu funkcji natychmiast (autowywołanie, ang. self-execution)

/* tutaj powinna być obsługa sessionStorage */

/* tutaj powinna być obsługa localStorage */

document.querySelector('#dzialanie button').addEventListener('click', e => {
	var odpowiedziArticle = document.querySelector('#odpowiedzi div div');
	odpowiedziArticle.innerHTML="";
	document.querySelectorAll("#poleOdpowiedzi div").forEach( pole => {
		odpowiedziArticle.innerHTML+=`<div><h4>Odpowiedź na pytanie: 
			"${pole.dataset.pytanie}"</h4>
			<p class='${pole.odp===pole.dataset.udzielonaOdp ? `poprawna'>jest poprawna i` : 
			`bledna'>jest błędna. Odpowiedź poprawna`} brzmi: 
			"${pole.dataset['odp'+pole.odp]}"</p>`;
		//poniższy kod zastąpiony powyższym
		/*if(pole.odp===pole.dataset.udzielonaOdp) {
			odpowiedziArticle.innerHTML+='<div><h4>Odpowiedź na pytanie: "' +
			pole.dataset.pytanie + '"</h4><p>jest poprawna i brzmi: "' + pole.dataset['odp'+pole.odp] +
			'"</p>';
		}
		else {
			odpowiedziArticle.innerHTML+='<div><h4>Odpowiedź na pytanie: "' +
			pole.dataset.pytanie + '"</h4><p>jest błędna. Odpowiedź poprawna brzmi: "' + pole.dataset['odp'+pole.odp] +
			'"</p>';
		}*/
	}); 
	odpowiedziArticle.parentNode.parentNode.style.display="block";
	setTimeout(() => {
	odpowiedziArticle.parentNode.parentNode.style="display:block;top:0;left:0;width:100%;height:100%;";
	}, 500);
});	

function odpowiedziClick(zdarzenie, obiekt) {
	if(!obiekt) return;
	
	obiekt.style="display: block;";
	setTimeout(() => {
		obiekt.style="";
	}, 2000);
	zdarzenie.stopPropagation();
}

{
	let odpowiedziArticle = document.querySelector('#odpowiedzi div');
	odpowiedziArticle.addEventListener('click', e => {
		odpowiedziClick(e, odpowiedziArticle.parentNode);
	},true);
	
	Array.from(odpowiedziArticle.children).forEach( dziecko => {
		dziecko.addEventListener('click', e => {
			odpowiedziClick(e)
		},true);
	});
}

/*
	setinterval to funkcja, która w przeciwieństwie do setTimeout działa
	w nieskończoność. Instrukcja zawarta w pierwszym parametrze będzie się
	wykonywać co określony czas (w milisekundach - drugi parametr).
	Najczęściej parametrem jest nazwa funkcji bądź definicja funkcji anonimowej.
	W poniższym przykładzie mamy do czynienia właśnie z funkcją anonimową. 
	
	setinterval() zwraca uchwyt (referencję) do miejsca w pamięci, w którym
	rezyduje (jest zapisana). Jeżeli uchwyt zapiszemy pod zmienną (jak poniżej
	- zmienna przerwanieCzas), to z kolei poprzez funkcję 
	clearInterval() możemy przerwać działanie takiej czasowej pętli (podając
	jako parametr uchywt do wspomnianej pętli).
	
	Ponieważ w HTMLu ładujemy skrypty z atrybutem async nie mamy pewności,
	kiedy potrzebny nam skrypt pytania.js zostanie załadowany przez przeglądarkę.
	W związku z powyższym poprzez poniższą pętlę odpytujemy skrypt, czy mamy
	dostęp do obiektu pytaniaOdpowiedzi; 
	
	Jeżeli będziemy mieli - kasujemy pętlę i generujemy pytania 
	(funkcja generujPytania());
*/
var przerwanieCzas = setInterval(() => {
							if (pytaniaOdpowiedzi) {
								if (przerwanieCzas) 
									clearInterval(przerwanieCzas);
								generujPytania();
							}
						},200);
						
