/*
	https://developer.mozilla.org/pl/docs/Web/API/Window/localStorage
*/

/*
	Działa identycznie do sessionStorage lecz zapisane tutaj dane są "wieczne", tj.
	usuwane dopiero wraz z pamięcią podręczną przeglądarki. Przykłady analogiczne do
	sessionStorage (zmiana nazw samych funkcji).
*/