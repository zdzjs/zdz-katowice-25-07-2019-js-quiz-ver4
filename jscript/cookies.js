/*
poniższe funkcje odpowiadają za ustawianie i kasowanie ciasteczek na stronie WWW
każdemu ciastku możemy ustawić datę "przydatności" (bez dany ciasteczka są
przechowywane do wyłączenia przeglądarki)
Usuwanie ciastka to podanie jego nazwy (z dowolną wartością) i 
jakąś datą przeszłą 
ZADANIE: Utworzyć funkcje ustawiania ciastek, pobierania (po jego nazwie)
oraz usuwania.
https://www.w3schools.com/js/js_cookies.asp
Konwencja ustawiania/kasowania ciastek - można przyjąć taką jak w PHP (bądź inną)
https://www.php.net/manual/en/features.cookies.php
https://www.w3schools.com/jsref/jsref_obj_date.asp
https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Date
*/
//document.cookie="uzytkownik=Jacek; expires=Fri, 20 Nov 2020 12:00:00 UTC";
//document.cookie="login=Gracz1; expires=Fri, 20 Nov 2020 12:00:00 UTC";
//console.log(document.cookie); //uzytkownik=Jacek; login=Gracz1; ciasteczko1=a; 
//document.cookie="uzytkownik=; expires=Fri, 20 Nov 1960 12:00:00 UTC";
//document.cookie="login=kasujeMnie; expires=Fri, 20 Nov 1960 12:00:00 UTC";

/**
	Funkcja ma za zadanie ustawiać ciasteczko. Parametry wejściowe:
	@name - nazwa ciasteczka, które będzie przechowywało wartość
	@value - wartość, którą chcemy w ciasteczku zapisać
	@time - czas życia ciasteczka, po którym zostanie ono usunięte z 
			komputera; czas podajemy w sekundach
		np. jeżeli chcemy by ciasteczko miało rok ważności, podajemy tutaj
		wartość 60*60*24*365 (60 sekund * 60 minut * 24 godziny * 365 dni roku)
*/
function setcookie(name, value, time) {
	document.cookie=`${name}=${value}; ${(()=>{let a=new Date();a.setMilliseconds(0);a.setMilliseconds(time*1000); return a.toGMTString();})()}`;
	/*
	// wersja najprostsza kodowo
		let data=new Date();
		//opcjonalne zerowanie milisekund
		data.setMilliseconds(0);
		data.setMilliseconds(time*1000);
		document.cookie=name+'='+value+'; '+data.toGMTString();
	//wersja z opisem wybranej metody - metoda została odpowiednio podzielona
	//na linie kodu celem lepszego zrozumienia jej działania:
	document.cookie=     //tutaj bez zmian - dostęp do ciasteczek przeglądarki dla naszej strony
	`${name}= 			//otwieramy szablon literału (`) i podajemy, by JS wstawiło nam w pole ${} wartość ze zmiennej name; znak równości to po prostu zwykły znak, który się doda do ciągu znakowego
	${value}; 		//we fragment ${} JS wstawi nam wartość zmiennej value (bez zadeklarowanego bloku mielibyśmy po prostu napis value; średnik to zwykły znak
	//poniżej funkcja rozbita na linijki celem jej szczegółowego omówienia
	${(  		//rozpoczynamy blok, w którym chcemy wstawić wartość zamiast wpisanego przez nas ciągu znakowego; rozpoczynamy okrągły nawias okrągły, którego znaczenie będzie wyjaśnione w dalszych liniach
	() => {  //deklarujemy funkcję anonimową; deklaracja tożsama z function() {; otwieramy blok ciała funkcji (nawias klamrowy)
	let a=new Date(); //dklarujemy zmienną o zasięgu lokalnym, do której przypisujemy nowo tworzny obiekt typu Date(); ponieważ nie podaliśmy dla Date żadnych parametrów obiekt a będzie przechowywał obecną datę zapisana z naszego systemu operacyjnego
	a.setMilliseconds(0); //zerowanie milisekund (jak w prostej wersji)
	a.setMilliseconds(time*1000); //jak w prostej wersji kodu
	return a.toGMTString(); //funkcja ma za zadanie zwrócić ciąg znakowy utworzony z zapisanej daty w określonym formacie (format daty dla ciasteczek)
	})             //zakończenie ciałą funkcji (klamra }) oraz zakończenie nawiasu okrągłego (rozpoczętego w linii 45); instrukcje wzięte w taki nawias traktowane są jako pojedyncza zmienna z punktu widzenia JS; w tym wypadku jest to "zmienna" funkcja 
	()}`           //bezpośrednio za zamknięciem obiektu funkcji mamy wywołane dwa nawiasy klamrowe. Dla JS jest to znak, że tak zapisana funkcja ma się natychmiast wykonać, co za tym idzie cały nasz blok po prostu zwróci ciąg znakowy z zapisaną dokładną datą
	
	Dlatego warto stosować taki zapis? Przede wszystkim działnie to separuje nam
	pamięć zmiannych użytych w funkcji od pamięci np. używanej w naszej funkcji głównej;
	Ponadto niektóre parsery potrafią tego typu zapis odpowiednio zoptymalizować
	przez co tak zapisany kod może się wykonać szybciej  niż tworzone przez nas
	zmienne w ciele pierwotnej funkcji.
	*/
}

function getcookie(name) {
	return (a => {for(let i=0;i<a.length;i++){let b=a[i].trim().split('=');if(b[0]==name)return b[1];}})((document.cookie).split(';'));
	/*
		(
			a => { //litera to po nic innego jak nazwa zmiennej; obecny zapis funkcji anonimowej pozwala na pomijanie () jeżeli funkcja przyjmuje przynajmniej jeden parametrów
			for(let i=0;a.length;i++) { //standardowy for
			let b= //utworzenie zmiennej lokalnej b 
			a[i]. //odowałnie się do wartości zmiennej przechowywanej w tablicy a pod indeksem z numer i 
			trim(). //funkjca usuwająca wszystkie białe znaki z ciągu znakowego (w ciasteczkach zapisywana jest spacja jako separator)
			split('=');  //wartość pozyskana z funkcji trim zostaje dodatkowo podzielona po znaku = (b będzie więcej tabicą z przynajmniej jednym elementem)
			if(b[0]==name) //sprawdzamy czy zapisana pod indeksem 0 wartość jest identyczna do wartości przekazanej jako parametr przez użytkownika
			return b[1];} jeżeli tak to zwracamy wartośc zapisaną pod indekstem 1; tym samym funkcja przerywa swój kod i kończy działanie
			}
		)    //kończymy ciało funkcji (}) oraz obiektu nawiasem )
		((document.cookie).split(';')) //ta linia działa do poprzednio tłumczonej (z samymi nawiasami () )
		//w tym wypadku jednak podajemy wartość wejściową tak wywływanej funkcji, który przyjmie wartość document.cookie (wszystkie nasze ciasteczka dla witryny)
		//ponieważ jest to wartość tekstowa, możemy ją rozdzielać jak każdy ciąg znakowy poprzez funkcję split; 
		//ciąg dzielimy po średniku, przez otrzymujemy tyle elementów w tworzonej tablicy, ile wartości ciasteczek znajduje się dla witryny
	//EKWIWALENT KODU
		function pobierzWartosc(a) {
			for(let i=0;a.length;i++) { 
				let b=a[i].trim().split('=');
				if(b[0]==name)
					return b[1];
			}
		}
		pobierzWartosc((document.cookie).split(';'));
	*/
}
/**
	Funkcja ma za zadnie wybrania WSZYSTKICH ustawionych ciasteczek dla strony
	i przypisanie ich do obiektu w stylu JSON, np.:
	{
		login: 'gracz1',
		imie: 'Jacek',
		wiek: 14
	}
	gdzie login, imie, wiek to nazwy ciasteczek, a wartości po dwukroptu to ich wartości
	Funkcja zwróci wartość false (Boolean) jeżeli nie ma żadnych ciasteczek
	dla strony
*/
function getcookiesAll() {
	var wyjscie ={};
	let ciastka = document.cookie.split(';').forEach( ciastko => {
		ciastko=ciastko.trim().split('=');
		wyjscie[ciastko[0]]=ciastko[1];
	});
	if (Object.keys(wyjscie)[0]==="") wyjscie=false;
	return wyjscie;
}

/**
	Funkcja kasuje ciasteczko wskazane przez name
	Ustawia datę z przeszłości
*/
function deletecookie(name) {
	document.cookie=`${name}=; expires=Fri, 1 Jan 1960 00:00:00 UTC`;
}

/**
	Funkcja umożliwiająca usunięcie wszystkich ciasteczek dla strony 
*/
function deletecookiesAll() {
	Object.keys(getcookiesAll()).forEach( k => {
		deletecookie(k);
	});
}

/*
Testujący kod dla funkcji obsługi cookies. 
Ustawiony czas to:
60 - ilość sekund
60 - ilość minut w godzinie
24 - ilość godzin w dobie
365 - ilość dni w roku (nie bierzemy pod uwagę daty przestępnej)

Łącznie czas życia ustawionych ciastek to 365 dni!
setcookie('ciastko', 'test', 60*60*24*365);
setcookie('ciastko2','strona', 60*60*24*365);
console.log(getcookie('uzytkownik'));
console.log(getcookie('ciastko'));
console.log(getcookiesAll());
deletecookie('uzytkownik');
console.log(getcookiesAll());
deletecookiesAll();
console.log(getcookiesAll());
*/