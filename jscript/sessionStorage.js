/*
	https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage
*/

/*
	Skład sesyjny działa tylko do momentu zamknięcia zakładki danej strony.
	Dzięki temu nie musimy, jak to było w ciasteczkach, ustawiać czasu życia zmiennych 
	(tam też można było pominąć ten parametr, ale tego nie robiliśmy).
	Jednak bez czasu ciasteczka żyły do przeładowania strony, zaś zmienne sesyjne
	pozostaną dalej.
	
	Przykłady użycia:
	setSessionValue('imie','Mieczysław');
	setSessionValue('nazwisko','Okoniewski');
	console.log(getSessionValue('wiek'));
	console.log(getSessionValue('imie'));
	console.log(getSessionValue('nazwisko'));
	console.log(getSessionValuesAll());
	deleteSessionValue('nazwisko');
	console.log(getSessionValuesAll());
	deleteSessionValuesAll();
	console.log(getSessionValuesAll());
	*/